package inheritance;

public class Student extends Person {
    String nim;
    int spp;
    int sks;
    int modul;

    public Student()
    {
        System.out.println("Inside Student:Constructor");
        super.name ="Anda";
    }

    public Student(String name, String address, String hobi, String nim)
    {
        super(name, address, hobi);
        this.nim = nim;
    }

    public String getNim()
    {
        return nim;
    }

    public int getSPP()
    {
        return spp;
    }

    public int GetSKS()
    {
        return sks;
    }

    public int getModul()
    {
        return modul;
    }

    //override
    public void identity()
    {
        super.identity();
        System.out.println("NIM : "+getNim());
    }

    public int hitungPembayaran(int spp , int sks , int modul)
    {
        this.spp = spp;
        this.sks = sks*250000;
        this.modul =modul*100000;

        return getSPP()+GetSKS()+getModul();
    }
}
