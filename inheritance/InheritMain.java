package inheritance;

import java.util.Scanner;

public class InheritMain {

    public static void main(String[] args) {
        String name;
        String address;
        String hobi;
        String  nim;
        int spp, sks, modul;

        //objek scanner
        Scanner input = new Scanner(System.in);

        //input data
        System.out.println("Masukkan Data");
        System.out.println("Nama :");
        name = input.next();
        System.out.println("Alamat : ");
        address = input.next();
        System.out.println("Hobi :");
        hobi = input.next();
        System.out.println("NIM : ");
        nim = input.next();
        System.out.println("SPP : ");
        spp = input.nextInt();
        System.out.println("SKS : ");
        sks = input.nextInt();
        System.out.println("Modul : ");
        modul = input.nextInt();

        //objek dari class Student
        Student Mahasiswa = new Student(name, address, hobi, nim);

        //Menampilkan data 
        System.out.println(" Data Mahasiswa ");
        Mahasiswa.identity();

        //total pembayaran
        System.out.println("Total pembayaran : "+Mahasiswa.hitungPembayaran(spp, sks, modul));
    }
}
